#!/usr/bin/expect -f
set username "pi"
set password "raspberry"
set file "/home/pi/.config/lxsession/LXDE-pi/autostart"
set url "http://10.0.0.31/infoscreen/cgi-bin/infoanzeige.pl?counter=4&PC=256"
spawn ssh $username@[lindex $argv 0]
expect {
 "assword:" { send -- "$password\r"}
 "authenticity" { 
	send -- "yes\r"
	expect -- "assword"
	send -- "$password\r" 
 }
}
expect "#"
send -- "sudo su\r"
expect "#"
send -- "init 6\r"
send -- "exit\r"
interact
