# maturastreich-2018

This repo contains all code and scripts used for the 2018 graduation prank at HTL St. Pölten.

The goal of the code in this repo was to take over the information screens which have been put up everywhere around the school and display our own content on them.

The deploy.sh script has been used to infect the hosts that run the screens and inject our own URL to the chromium instances that ran on them.
The new URL pointed to a local Laravel application with a Node.js server that allowed us to remotely change the URL of an full-screen iFrame that then ran on the now infected monitors.
This way we were able to control the content that is being shown on every screen individually.


! This code is not pretty! It's the bare minimum to make the whole thing work without any nice UI. I had only one afternoon to complete this whole project so don't be too annoyed at possible bugs.

! This repo is intented for demonstration purposes only !