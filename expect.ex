#!/usr/bin/expect -f
set username "pi"
set password "raspberry"
set file "/home/pi/.config/lxsession/LXDE-pi/autostart"
set url "http://10.117.252.172:8000/random" # The URL to change the inforscreens to
spawn ssh $username@[lindex $argv 0]
expect {
 "assword:" { send -- "$password\r"}
 "authenticity" { 
	send -- "yes\r"
	expect -- "assword"
	send -- "$password\r" 
 }
}
expect "~"
send -- "sudo su\r"
expect "#"
send -- "> /home/pi/.config/lxsession/LXDE-pi/autostart\r"
expect "#"
send -- "touch $file\r"
expect "#"
send -- "echo '@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
@xscreensaver -no-splash
@xset s off
@xset -dpms
@xset s noblank
@/usr/bin/chromium-browser --noerrdialogs --incognito --kiosk --no-first-run --disable --disable-translate --disable-infobars --disable-suggestions-service --disable-save-password-bubble --disk-cache-dir=/tmp/chromium/cache/ --user-data-dir=/tmp/chromium/user_data/ --remote-debugging-port=4444 $url' >> $file\r"
expect "#"
send -- "killall lxpanel\r"
expect "#"
send -- "exit\r"
expect "~"
send -- "export DISPLAY=:0.0\r"
expect "~"
send -- "startlxde-pi &\r"
expect "~"
send -- "exit\r"
interact
