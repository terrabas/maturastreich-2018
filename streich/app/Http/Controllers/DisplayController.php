<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DisplayController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('display');
    }

    public function randomImg(){
        $files = Storage::disk('public')->allFiles('img');
        $randomFile = $files[rand(0, count($files)-1)];
        $file = Storage::disk('public')->get($randomFile);
        return response($file)->header('Content-Type','image/jpeg');
    }

    public function random(){
        return view('random');
    }
}
