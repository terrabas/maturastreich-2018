<?php

namespace App\Http\Controllers;

use App\Helpers\NodeHelper;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $connected = true;
        $connections = NodeHelper::getFromNode('/connections');
        if($connections == null)
            $connected = false;
        return view('home',[
            'connected' => $connected,
            'connections' => $connections
        ]);
    }

    public function changeURL(Request $req, $id){
        $url = $req->get('toUrl');
        $success = NodeHelper::makePostRequest('/changeURL', json_encode(['id' => $id, 'url' => $url, 'autoplay' => $req->get('autoplay')]));
        if(!$success)
            return redirect()->back()->withErrors(['Website change failed!']);
        return redirect('/home');
    }
}
