<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 21.02.2018
 * Time: 21:59
 */

namespace App\Helpers;

use GuzzleHttp\Client;

class NodeHelper
{
    public static function getFromNode($PATH){
        $client = new Client();
        try {
            $res = $client->get('http://localhost:3000' . $PATH);
            $resp = json_decode($res->getBody());
        } catch (\Exception $e) {
            return null;
        }
        return $resp;
    }

    public static function makePostRequest($path, $body)
    {
        $success = false;
        $client = new Client();
        try {

            $res = $client->post('http://localhost:3000' . $path, ['body' => $body, 'headers'=>['Content-Type' => 'application/x-www-form-urlencoded' ]]);
        } catch (\Exception $s){
            return false;
        }
        try {
            $response = json_decode((string)$res->getBody());
            if (isset($response->success))
                // If chat server returns a success message, use it's content so see if the operation has failed or succeeded
                $success = $response->success;
        } catch (\Exception $e) {
            // If an error in conversion occurs, the chat server probably wasn't replying with a valid JSON, so let's say it's okay
            $success = true;
        }
        return $success;
    }
}