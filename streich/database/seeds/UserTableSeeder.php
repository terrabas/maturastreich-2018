<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => '5BHMBA',
            'email' => '5BHMBA@maturastreich.at',
            'password' => bcrypt('maturastreich'),
        ]);
    }
}
