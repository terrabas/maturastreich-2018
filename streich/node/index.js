var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

const bodyParser = require('body-parser');
// configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

var connections = [];
var name = [];
var site = [];


io.on('connection', function (socket) {
    console.log('A client with IP ' + socket.handshake.address + ' connected!');
    connections.push(socket);
    console.log("Resolving...");
    name.push(reverseLookup(socket.handshake.address));
    console.log("Sending URL change...");
    //changeWebsite(socket, "https://en.wikipedia.org/wiki/Main_Page");
    console.log("Done");
    console.log("Binding disconnecter...");
    socket.on('disconnect', function() {
        console.log('A client with IP ' + socket.handshake.address + ' disconnected! Cleaning up...');
        clearUpConnections(socket);
        clearUpName(socket);
        clearUpSite(socket);
        console.log("Cleanup complete!");
    });
});

function changeWebsite(socket, url) {
    console.log("Switching website...");
    socket.emit("website change", url);

    for (i = 0; i < site.length; i++) {
        if (site[i] != null && site[i].id == socket.id) {
            site[i].url = url;
            return;
        } else if (site[i] == null) {
            site[i] = {id: socket.id, url: url};
            return;
        } else {
            // Do nothing, keep array that way
        }
    }

    site.push({id: socket.id, url: url});
}

function changeWebsiteAll(url) {
    console.log("Switching website...");

    for (i = 0; i < connections.length; i++ ) {
        connections[i].emit('website change', url);
    }

    for (y = 0; y < site.length; y++) {
        if (site[y] != null) {
            site[y].url = url;
        } else {
            // Do nothing, keep array that way
        }
    }
    console.log(JSON.stringify(site));
}

function clearUpConnections(socket) {
    for (i = 0; i < connections.length; i++) {
        if(connections[i].id == socket.id)
            connections.splice(i,1);
    }
}

function clearUpName(socket) {
    for (i = 0; i < name.length; i++) {
        if(name[i] != null && name[i].id == socket.id)
            name.splice(i,1);
    }
}

function clearUpSite(socket) {
    for (i = 0; i < site.length; i++) {
        if(site[i] != null && site[i].id == socket.id)
            site.splice(i,1);
    }
}

http.listen(3000, function () {
    console.log('listening on *:3000');
});

app.get('/connections', function (req, res) {

    res.send(JSON.stringify(getConnectionInfo()));
});

app.get('/test', function (req, res) {

    res.send(JSON.stringify(site));
});

app.post('/changeURL', function (req, res) {
    console.log(req.body);
    req.body = JSON.parse(Object.keys(req.body)[0]);
    console.log("ChangeURL request received for socket ID "+req.body.id+" and website "+req.body.url+" !");
    if(req.body.id == "*"){
        console.log("Changing website for all clients...");
        if(req.body.autoplay)
            req.body.url = req.body.url+"?autoplay=1&loop=1";
        changeWebsiteAll(req.body.url);
        res.send(JSON.stringify({success: true}));
        console.log("Website switch complete!");
        return;
    }
    thisSocket = socketWhereID(req.body.id);
    if(thisSocket == null) {
        console.log("Socket is null!");
        res.send(JSON.stringify({success: false}));
        return;
    } else{
        if(req.body.autoplay)
            req.body.url = req.body.url+"?autoplay=1&loop=1";
        changeWebsite(thisSocket, req.body.url);
        res.send(JSON.stringify({success: true}));
        console.log("Website switch complete!");
        return;
    }
});


function getConnectionInfo() {
    temp = [];
    console.log("Looping through: "+connections.length);
    for (i = 0; i < connections.length; i++) {
        local = {};
        local['ip'] = connections[i].handshake.address;
        local['id'] = connections[i].id;
        try {
            if (name[i] == "" || name[i] == null)
                local['name'] = "N/A";
            else
                local['name'] = name[i];
        } catch (err) {
            local['name'] = 'N/A';
        }
        localsite = siteWhereSocketID(connections[i]);
        if (localsite == null)
            local['url'] = "None";
        else
            local['url'] = localsite.url;
        temp.push(local);
    }
    console.log("Done!");
    return temp;
}

function socketWhereID(id) {
    if(connections != null){
        for (i = 0; i < connections.length; i++) {
            if(connections[i].id == id)
                return connections[i];
        }
    }
    return null
}

function siteWhereSocketID(socket) {
    if (site != null) {
        for (z = 0; z < site.length; z++) {
            console.log("Looping through site...");
            if (site[z] != null && site[z].id == socket.id) {
                return site[z];
            }
        }
        return null;
    }
    return null;
}


function pluck(array, key) {
    return array.map(function (obj) {
        return obj[key];
    });
}


function reverseLookup(ip) {
    require('dns').reverse(ip, function (err, domains) {
        if (err) {
            console.log(err.toString());
            return null;
        }
        return console.log(domains[0]);
    });
};