<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>

    <script>
        function loadIframe(iframeName, url) {
            var $iframe = $('#' + iframeName);
            if ( $iframe.length ) {
                $iframe.attr('src',url);    // here you can change src
                return false;
            }
            return true;
        }

        $(function () {
            var socket = new io(window.location.hostname + ":3000");
            socket.on('website change', function (msg) {
                loadIframe('website',msg);
            });
        });
    </script>

    <title>It works!</title>

    <!-- Styles -->
    <style>
        html, body {
            background-color: #000000;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }
    </style>
</head>
<body>
<iframe id="website" src="http://localhost:8000/" width="100%" height="100%" frameborder="0"></iframe>
</body>
</html>
