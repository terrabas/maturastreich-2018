@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        <li>{{ $errors->first() }}</li>
                                    </ul>
                                </div>
                            @endif

                        @if($connected)
                        Connected clients
                        <br>
                            @foreach($connections as $connection)
                                <div>
                                {{ $connection->ip }}
                                {{ $connection->name }}
                                {{ $connection->url }}
                                <form method="POST" style="display: inline; float: right" action="{!! action('HomeController@changeURL',['id' => $connection->id]) !!}">
                                    {{ csrf_field() }}
                                    <input type="text" id="toUrl" name="toUrl" placeholder="New URL" style="display: inline">
                                    <input type="checkbox" id="autoplay" name="autoplay" style="display: inlinen">
                                    <input type="submit" value="Change URL">
                                </form>
                                </div>
                                <br><br><br>
                            @endforeach
                            <br><br><br>
                                <form method="POST" action="{!! action('HomeController@changeURL',['id' => '*']) !!}">
                                    {{ csrf_field() }}
                                    <input type="text" id="toUrl" name="toUrl" placeholder="New URL" style="display: inline">
                                    <input type="checkbox" id="autoplay" name="autoplay" style="display: inlinen">
                                    <input type="submit" value="Change URL on all clients">
                                </form>
                            @else
                            Node not running or no clients connected!
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
