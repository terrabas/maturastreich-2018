#!/bin/sh
echo "-- Maturastreich UNDeployer --"
echo ""
echo "UNDeploying..."
for ip in `cat IPs.list`; do
	./unexpect.ex $ip
	echo "-- !$ip CLEARED! --"
done
echo "Done!"
